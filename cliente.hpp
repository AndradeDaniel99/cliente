#include <string>

using namespace std;

class Cliente {
private:
    // Atributos
    string nome;
    int idade;
//    long int cpf;
    string cpf;
    string email;
public:
    // Métodos
    Cliente();  // Construtor
    ~Cliente(); // Destruror
    // Métodos acessores
    void set_nome(string nome);
    string get_nome();
    void set_idade(int idade);
    int get_idade();
    void set_cpf(long int cpf);
    long int get_cpf();
    //void set_cpf(string cpf);
    //string get_cpf();
    void set_email(string email);
    string get_email();
    // Outros Métodos
    void imprime_dados();

};















