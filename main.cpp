#include <iostream>
#include "cliente.hpp"

using namespace std;

int main(int argc, char ** argv){

    Cliente cliente1;
    Cliente cliente2;

    cliente2.nome = "Felipe";
    cliente2.idade = 3;
    cliente2.cpf = 234234234;
    cliente2.email = "felipe@email.com";
    cout << "Dados do cliente2 " << endl;
    cliente2.imprime_dados();
    cout << endl;


    Cliente * cliente3;
    cliente3 = new Cliente();    

    Cliente * cliente4;

    cliente1.imprime_dados();
    cliente1.set_nome("João");
    cliente1.set_idade(21);
    cliente1.set_cpf(1234123234);
    cliente1.set_email("joao@email.com");
    cout << "Após atualização de atributos" << endl;
    cliente1.imprime_dados();
    
    cout << "Dados do cliente3" << endl;
    cliente3->set_nome("Paulo");
    cliente3->set_idade(19);
    cliente3->set_cpf(42232212345);
    cliente3->set_email("paulo@email.com");
    cliente3->imprime_dados();

    cout << "Alocação de objetos em ponteiros" << endl;
    cliente4 = &cliente1;
    cliente4->imprime_dados();
    cliente4 = cliente3;
    cliente4->imprime_dados();

    delete cliente3;

    return 0;
}






